﻿function XHRManager(settings)
{
    if (!settings)
    {
        throw new Error("Required parameter is missing");
    }

    if (settings.Success)
    {
        this.OnSuccess = settings.Success;
    }

    if (settings.Error)
    {
        this.OnError = settings.Error;
    }

    if (settings.Abort)
    {
        this.OnAbort = settings.Abort;
    }

    if (settings.Type)
    {
        this._Type = settings.Type;
    }

    if (settings.Url)
    {
        this._Url = settings.Url;
    }

    if (settings.Async === false)
    {
        this._Async = false;
    }
    else
    {
        this._Async = true;
    }


    this._Opened = false;
    this._init();
}

var xhrProto = XHRManager.prototype;

//#region handlers
xhrProto._onReadyStateChange = function ()
{
    var xhrManager = this.self;
    if (!xhrManager)
        return;

    switch (this.readyState)
    {
        case 0: //CREATED
            break;
        case 1: //OPENED
            break;
        case 2: //SENDED
            break;
        case 3: //RECEIVED
            break;
        case 4: //LOADED
            switch (this.status)
            {
                case 200: //SUCCESS
                    if (xhrManager.OnSuccess)
                    {
                        xhrManager.OnSuccess({ ResponseText: this.responseText });
                    }
                    break;
                case 404: //NOTFOUND
                case 500: //ERROR
                    if (xhrManager.OnError)
                    {
                        xhrManager.OnError({ ResponseText: this.responseText, Status: this.status });
                    }
                    break;
                case 0: //ABORT
                    if (xhrManager.OnAbort)
                    {
                        xhrManager.OnAbort({ ResponseText: 'ABORT', Status: this.status });
                    }
                    break;
            }
            delete this.self;
            break;
    }
};
//#endregion handlers

//#region protected
xhrProto._init = function ()
{
    this._XHR = new window.XMLHttpRequest();
    this._XHR.self = this;
    this._XHR.onreadystatechange = this._onReadyStateChange.bind(this._XHR);
};
//#endregion protected

//#region public
xhrProto.send = function (body)
{
    if (body && $u.isObject(body))
    {
        body = JSON.stringify(body);
    }

    if (!this._Opened)
    {
        this._XHR.open(this._Type, this._Url, this._Async);
        this._Opened = true;
    }

    this._XHR.setRequestHeader("Accept", "text/javascript");
    this._XHR.setRequestHeader('Content-Type', 'application/json;charset=utf-8');

    if (body)
    {
        this._XHR.send(body ? body : this._Body);
    }
    else
    {
        this._XHR.send();
    }
};
//#endregion public

xhrProto = null;
