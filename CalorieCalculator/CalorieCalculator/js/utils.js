﻿String.prototype.format = function (args)
{
    var str = this;
    return str.replace(String.prototype.format.regex, function (item)
    {
        var intVal = parseInt(item.substring(1, item.length - 1));
        var replace;
        if (intVal >= 0)
        {
            replace = args[intVal];
        } else if (intVal === -1)
        {
            replace = "{";
        } else if (intVal === -2)
        {
            replace = "}";
        } else
        {
            replace = "";
        }
        return replace;
    });
};
String.prototype.format.regex = new RegExp("{-?[0-9]+}", "g");

try
{
    new CustomEvent("IE has CustomEvent, but doesn't support constructor");
} catch (e)
{
    window.CustomEvent = function (event, params)
    {
        var evt;
        params = params || {
            bubbles: false,
            cancelable: false,
            detail: undefined
        };
        evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    };

    CustomEvent.prototype = Object.create(window.Event.prototype);
}

(function ()
{
    function addClass(obj, cls)
    {
        var classes = obj.className ? obj.className.split(' ') : [];

        for (var i = 0; i < classes.length; i++)
        {
            if (classes[i] == cls) return;
        }

        classes.push(cls);
        obj.className = classes.join(' ');
    }

    function removeClass(obj, cls)
    {
        var classes = obj.className ? obj.className.split(' ') : [];

        for (var i = (classes.length - 1) ; i >= 0; i--)
        {
            if (classes[i] == cls)
            {
                classes.splice(i, 1);
            }
        }

        obj.className = classes.join(' ');
    }

    function getChar(event)
    {
        if (event.which == null)
        {
            if (event.keyCode < 32) return null;
            return String.fromCharCode(event.keyCode) // IE
        }

        if (event.which != 0 && event.charCode != 0)
        {
            if (event.which < 32) return null;
            return String.fromCharCode(event.which) // остальные
        }

        return null; // специальная клавиша
    }

    function changeText(elem, changeVal)
    {
        if (typeof elem.textContent !== "undefined")
        {
            elem.textContent = changeVal;
        }
        else
        {
            elem.innerText = changeVal;
        }
    }

    function isObject(v)
    {
        return !!v && Object.prototype.toString.call(v) === '[object Object]';
    };

    window.$u = {
        addClass: addClass,
        removeClass: removeClass,
        getChar: getChar,
        changeText: changeText,
        isObject: isObject
    };
})()