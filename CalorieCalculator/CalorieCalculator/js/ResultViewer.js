﻿function ResultViewer(settings)
{
    if (settings)
    {
        if (settings.BackBtnClicked)
        {
            this._BackBtnClicked = settings.BackBtnClicked;
        }

        if (settings.ParentNode)
        {
            this._ParentNode = settings.ParentNode;
        }
    }
}
var resProto = ResultViewer.prototype;

//#region handlers
resProto._onBackBtnClicked = function (e)
{
    if (this._BackBtnClicked)
    {
        this._BackBtnClicked();
    }
};
//#endregion handlers

//#region protected
resProto._bindEvents = function ()
{
    this._backBtn.addEventListener("click", this._onBackBtnClicked.bind(this), false);
};

resProto._render = function ()
{
    this._domNode = document.createElement('div');
    $u.addClass(this._domNode, "ResultViewer");

    this._domNode.innerHTML =
    '<div class="BackBtn" id="ResultBackBtn"><span>←</span>&nbsp;Back</div>' +
    '<div class="CalcRow">' +
        '<h1 id="CalcTitle" class="CalcH1">Daily kilocalorie requirements</h1>' +
    '</div>' +
    '<div class="CalcRow">' +
        '<p class="Pararaph"><span class="ResultText">Recommended daily kilocalorie intake to maintain current weight:</span>&nbsp;<span class="CalorieResultSpan" id="MainteinWeightSpan"></span></p>' +
        '<p class="Pararaph"><span class="ResultText">Recommended daily kilocalorie intake to lose weight in a healthy way:</span>&nbsp;<span class="CalorieResultSpan" id="LoseWeightSpan"></span></p>' +
    '</div>';

    this._backBtn = this._domNode.querySelector("#ResultBackBtn");
    this._mainteinWeightCtrl = this._domNode.querySelector("#MainteinWeightSpan");
    this._loseWeightCtrl = this._domNode.querySelector("#LoseWeightSpan");

    this._bindEvents();

    if (this._ParentNode)
    {
        this._ParentNode.appendChild(this._domNode);
    }
}
//#endregion protected

//#region public
resProto.init = function ()
{
    this._render();
};

resProto.update = function (info)
{
    if (!info || !info.MaintainWeight || !info.LoseWeight)
    {
        $u.changeText(this._mainteinWeightCtrl, "");
        $u.changeText(this._loseWeightCtrl, "");
    }
    else
    {
        $u.changeText(this._mainteinWeightCtrl, info.MaintainWeight);
        $u.changeText(this._loseWeightCtrl, info.LoseWeight);
    }
};
//#endregion public

resProto = null;