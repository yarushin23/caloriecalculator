﻿function AppManager(settings)
{
    if (settings)
    {
        if (settings.ParentNode)
            this._ParentNode = settings.ParentNode;
    }

    this._Template = 
    '<div class="CalculatorLayout" id="CalculatorLayout">'+
        '<div class="Front"></div>'+
        '<div class="Back"></div>' +
    '</div>';
}

var appProto = AppManager.prototype;

//#region handlers
appProto._onBackBtnClicked = function ()
{
    $u.removeClass(this._calculatorLayout, 'Flip');
};

appProto._onCalculateBtnClicked = function ()
{
    var formInfo = this._calcViewer.getData(),
        bmr;

    // Если данных нет, то и считать ничего не надо
    if (!formInfo)
    {
        return;
    }

    this._calcViewer.setEnableToCalculateBtn(false);
    this._calculateByHBFormulaOnServer(formInfo);

    // версия без ajax
    //metabolicInfo = this._calculateByHBFormula(formInfo)
    //this._showResult(metabolicInfo);
};

appProto._onGetMetabolicInfoSuccess = function (response)
{
    this._calcViewer.setEnableToCalculateBtn(true);
    if (!response || !response.ResponseText)
    {
        return;
    }

    var metabolicInfo = JSON.parse(response.ResponseText);
    this._showResult(metabolicInfo);
};

appProto._onXHRError = function (response)
{
    this._calcViewer.setEnableToCalculateBtn(true);
    if (!response || response.ResponseText)
    {
        return;
    }

    resp = JSON.parse(response.ResponseText);
    console.error(resp.Status + ":" + resp.ResponseText);
};
//#endregion handlers

//#region protected
appProto._calculateByHBFormulaOnServer = function (formInfo)
{
    var ajax = new XHRManager({
        Type: "POST",
        Url: "api/HBFormula/GetMetabolicInfo",
        Success: this._onGetMetabolicInfoSuccess.bind(this),
        Error: this._onXHRError.bind(this),
        Abort: this._onXHRError.bind(this)
    });

    ajax.send(formInfo);
};

appProto._calculateByHBFormula = function (info)
{
    /// <summary>
    /// Расчитывает по формуле Харриса Бенедикта основной обмен веществ
    /// и возвращает объект описывающий сколько киллокалорий необходимо, чтобы поддерживать текущий вес,
    /// и сколько необходимо, чтобы терять вес без вреда для здоровья
    /// </summary>

    var maintainCurrWeight, loseWeight;

    switch(info.Gender)
    {
        case "Male":
            maintainCurrWeight = (88.362 + (13.397 * info.Weight) + (4.799 * info.Height) - (5.677 * info.Age)) * info.Exercise;
            break;
        case "Female":
            maintainCurrWeight = (447.593 + (9.247 * info.Weight) + (3.098 * info.Height) - (4.330 * info.Age)) * info.Exercise;
            break;
        default:
            throw new Error("Invalid argument");
            break;
    }

    loseWeight = maintainCurrWeight * 0.8;

    return {
        MaintainWeight: Math.round(maintainCurrWeight),
        LoseWeight: Math.round(loseWeight)
    };
};

appProto._showResult = function (metabolicInfo)
{
    if (!metabolicInfo)
    {
        return;
    }

    this._resultViewer.update(metabolicInfo);

    $u.addClass(this._calculatorLayout, 'Flip');
};

appProto._render = function ()
{
    this._domNode = document.createElement('div');
    $u.addClass(this._domNode, "AppLayout");

    this._domNode.innerHTML = this._Template;
    this._calculatorLayout = this._domNode.querySelector(".CalculatorLayout");
    this._frontSideNode = this._domNode.querySelector(".Front");
    this._backSideNode = this._domNode.querySelector(".Back");

    if (this._ParentNode)
    {
        this._ParentNode.appendChild(this._domNode);
    }
};
//#endregion protected

//#region public
appProto.init = function ()
{
    this._render();
    this._calcViewer = new CalcViewer({
        ParentNode: this._frontSideNode,
        CalculateBtnClicked: this._onCalculateBtnClicked.bind(this)
    });
    this._calcViewer.init();

    this._resultViewer = new ResultViewer({
        ParentNode: this._backSideNode,
        BackBtnClicked: this._onBackBtnClicked.bind(this)
    });
    this._resultViewer.init();
};
//#endregion public

appProto = null;


