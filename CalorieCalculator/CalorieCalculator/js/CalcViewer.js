﻿function CalcViewer(settings)
{
    if (settings)
    {
        if (settings.ParentNode)
            this._ParentNode = settings.ParentNode;
        if (settings.CalcTitleText)
            this._CalcTitleText = settings.CalcTitleText;
        if (settings.GenderLblText)
            this._GenderLblText = settings.GenderLblText;
        if (settings.ExerciseLblText)
            this._ExerciseLblText = settings.ExerciseLblText;
        if (settings.WeightLblText)
            this._WeightLblText = settings.WeightLblText;
        if (settings.HeightLblText)
            this._HeightLblText = settings.HeightLblText;
        if (settings.AgeLblText)
            this._AgeLblText = settings.AgeLblText;
        if (settings.CalcBtnLblText)
            this._CalcBtnLblText = settings.CalcBtnLblText;
        if (settings.CalculateBtnClicked)
            this._CalculateBtnClicked = settings.CalculateBtnClicked;
    }

    this._IsFormValid = false;
    this._IsCalculateBtnEnabled = true;

    this._Template =
    '<div class="CalcRow">' +
        '<h1 id="CalcTitle" class="CalcH1">{0}</h1>' +
    '</div>' +
    '<div class="CalcRow">' +
        '<label class="GenderLbl CalcLbl CursorPointer" for="CalcGender">{1}</label>' +
        '<select class="GenderSelect CalcSelect CursorPointer" id="CalcGender">' +
            '<option value="Male" selected>Male</option>' +
            '<option value="Female">Female</option>' +
        '</select>' +
    '</div>' +
    '<div class="CalcRow">' +
        '<label class="ExerciseLbl CalcLbl CursorPointer" for="CalcExercise">{2}</label>' +
        '<select class="ExerciseSelect CalcSelect CursorPointer" id="CalcExercise">' +
            '<option value="1.2" selected>No exercise</option>' +
            '<option value="1.375">1-3 days per week</option>' +
            '<option value="1.55">3-5 days per week</option>' +
            '<option value="1.725">6-7 days per week</option>' +
            '<option value="1.9">Twice per day</option>' +
        '</select>' +
    '</div>' +
    '<div class="CalcRow">' +
        '<label class="WeightLbl CalcLbl CursorPointer" for="CalcWeight">{3}</label>' +
        '<input class="WeightInput CursorPointer CalcInput" id="CalcWeight" />' +
    '</div>' +
    '<div class="CalcRow">' +
        '<label class="HeightLbl CalcLbl CursorPointer" for="CalcHeight">{4}</label>' +
        '<input class="HeightInput CursorPointer CalcInput" id="CalcHeight" />' +
    '</div>' +
    '<div class="CalcRow">' +
        '<label class="AgeLbl CalcLbl CursorPointer" for="CalcAge">{5}</label>' +
        '<input class="AgeInput CursorPointer CalcInput" id="CalcAge" />' +
    '</div>' +
    '<div class="CalcRow">' +
        '<div id="CalculateBtn" class="Btn Btn-primary">' +
            '{6}' +
        '</div>' +
    '</div>';
}
var calcProto = CalcViewer.prototype;

calcProto._CalcTitleText = "Calculator of calories";
calcProto._GenderLblText = "Gender:";
calcProto._ExerciseLblText = "Exercise:";
calcProto._WeightLblText = "Weight (kg):";
calcProto._HeightLblText = "Height  (cm):";
calcProto._AgeLblText = "Age (years):";
calcProto._CalcBtnLblText = "Calculate";

//#region handlers
calcProto._onAgeInputBlurred = function (e)
{
    if (!this._isValidRequiredField(this._ageInput))
    {
        this._IsFormValid = false;
    }
};

calcProto._onWeightInputBlurred = function (e)
{
    if (!this._isValidRequiredField(this._weightInput))
    {
        this._IsFormValid = false;
    }
};

calcProto._onHeightInputBlurred = function (e)
{
    if (!this._isValidRequiredField(this._heightInput))
    {
        this._IsFormValid = false;
    }
};

calcProto._onAgeInputKeyPressed = function (e)
{
    var self = this;
    this._validateEventInt(e, this._ageInput, 150);
    setTimeout(function () { self._isValidRequiredField.call(self, self._ageInput); }, 0);
};

calcProto._onWeightInputKeyPressed = function (e)
{
    var self = this;
    this._validateEventInt(e, this._weightInput, 350);
    setTimeout(function () { self._isValidRequiredField.call(self, self._weightInput); }, 0);
}

calcProto._onHeightInputKeyPressed = function (e)
{
    var self = this;
    this._validateEventInt(e, this._heightInput, 350);
    setTimeout(function () { self._isValidRequiredField.call(self, self._heightInput); }, 0);
}

calcProto._onCalculateBtnClicked = function (e)
{
    if (!this._IsCalculateBtnEnabled)
        return;

    this._validateForm();

    if (this._IsFormValid && this._CalculateBtnClicked)
        this._CalculateBtnClicked();
};
//#endregion handlers

//#region protected
calcProto._isValidRequiredField = function (field)
{
    var isValid = true;

    if (!field.value)
    {
        isValid = false;
        $u.addClass(field, "Required");
    }
    else
    {
        $u.removeClass(field, "Required");
    }

    return isValid;
};

calcProto._validateForm = function ()
{
    var isValid = true;
    if (!this._isValidRequiredField(this._ageInput))
    {
        isValid = false;
    }

    if (!this._isValidRequiredField(this._weightInput))
    {
        isValid = false;
    }

    if (!this._isValidRequiredField(this._heightInput))
    {
        isValid = false;
    }

    this._IsFormValid = isValid;
};

calcProto._validateEventInt = function (e, sender, maxvalue)
{
    var chr, oldValue, newValue;

    e = e || event;

    if (e.ctrlKey || e.altKey || e.metaKey) return;

    chr = $u.getChar(e);

    // с null надо осторожно в неравенствах,
    // т.к. например null >= '0' => true
    // на всякий случай лучше вынести проверку chr == null отдельно
    if (chr == null) return;

    oldValue = sender.value;
    newValue = +(oldValue + chr);

    if (chr < '0' || chr > '9' || (!isNaN(newValue) && (newValue > maxvalue || newValue <= 0)))
    {
        e.preventDefault();
    }
};

calcProto._bindCalculateBtn = function ()
{
    this._calculateBtn.addEventListener("click", this._onCalculateBtnClicked.bind(this), false);
};

calcProto._bindAgeInput = function ()
{
    this._ageInput.addEventListener("keypress", this._onAgeInputKeyPressed.bind(this), false);
    this._ageInput.addEventListener("blur", this._onAgeInputBlurred.bind(this), false);
};

calcProto._bindWeightInput = function ()
{
    this._weightInput.addEventListener("keypress", this._onWeightInputKeyPressed.bind(this), false);
    this._weightInput.addEventListener("blur", this._onWeightInputBlurred.bind(this), false);
};

calcProto._bindHeightInput = function ()
{
    this._heightInput.addEventListener("keypress", this._onHeightInputKeyPressed.bind(this), false);
    this._heightInput.addEventListener("blur", this._onHeightInputBlurred.bind(this), false);
};

calcProto._bindEvents = function ()
{
    this._bindWeightInput();
    this._bindHeightInput();
    this._bindAgeInput();
    this._bindCalculateBtn();
};

calcProto._render = function ()
{
    var row, title;

    this._domNode = document.createElement('form');
    $u.addClass(this._domNode, "CalculatorForm");

    viewString = this._Template.format([this._CalcTitleText, this._GenderLblText, this._ExerciseLblText,
        this._WeightLblText, this._HeightLblText, this._AgeLblText, this._CalcBtnLblText]);

    this._domNode.innerHTML = viewString;
    this._genderSelect = this._domNode.querySelector("#CalcGender");
    this._exerciseSelect = this._domNode.querySelector("#CalcExercise");
    this._weightInput = this._domNode.querySelector("#CalcWeight");
    this._heightInput = this._domNode.querySelector("#CalcHeight");
    this._ageInput = this._domNode.querySelector("#CalcAge");
    this._calculateBtn = this._domNode.querySelector("#CalculateBtn");

    this._bindEvents();

    if (this._ParentNode)
    {
        this._ParentNode.appendChild(this._domNode);
    }
};
//#endregion protected

//#region public
calcProto.setEnableToCalculateBtn = function (enable)
{
    if (enable)
    {
        this._IsCalculateBtnEnabled = true;
        $u.removeClass(this._calculateBtn, "Disabled");
    }
    else
    {
        this._IsCalculateBtnEnabled = false;
        $u.addClass(this._calculateBtn, "Disabled");
    }
};

calcProto.init = function ()
{
    this._render();
};

calcProto.getData = function ()
{
    this._validateForm();
    //если форма полностью не заполнена, то ничего не отдаем
    if (!this._IsFormValid)
    {
        return null;
    }

    return {
        Gender: this._genderSelect.value,
        Exercise: +this._exerciseSelect.value,
        Weight: +this._weightInput.value,
        Height: +this._heightInput.value,
        Age: +this._ageInput.value
    };
};
//#endregion public

calcProto = null;