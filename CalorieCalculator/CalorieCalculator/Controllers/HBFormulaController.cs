﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;

namespace CalorieCalculator.Controllers
{
	public class HBFormulaController : ApiController
	{
		// POST api/values
        [HttpPost]
		public HttpResponseMessage GetMetabolicInfo(FormInfo formInfo)
		{
            //{"Gender":"Male","Exercise":1.2,"Weight":1,"Height":2,"Age":3}
            double maintainCurrWeight;
            switch (formInfo.Gender)
            {
                case Gender.Female:
                    maintainCurrWeight = (447.593 + (9.247 * formInfo.Weight) + (3.098 * formInfo.Height) - (4.330 * formInfo.Age)) * formInfo.Exercise;
                    break;
                case Gender.Male:
                    maintainCurrWeight = (88.362 + (13.397 * formInfo.Weight) + (4.799 * formInfo.Height) - (5.677 * formInfo.Age)) * formInfo.Exercise;
                    break;
                default:
                    return Request.CreateResponse<string>(HttpStatusCode.InternalServerError, "Invalid parameter 'Gender'");
                    break;
            }

            MetabolicInfo metabolicInfo = new MetabolicInfo() {
                MaintainWeight = Convert.ToInt32(maintainCurrWeight),
                LoseWeight = Convert.ToInt32(maintainCurrWeight * 0.8)
            };

            string result = JsonConvert.SerializeObject(metabolicInfo);

            return Request.CreateResponse<MetabolicInfo>(HttpStatusCode.OK, metabolicInfo);
		}
	}

    public class MetabolicInfo
    {
        public int MaintainWeight { get; set; }
        public int LoseWeight { get; set; }
    }

    public class FormInfo
    {
        public Gender Gender { get; set; }
        public double Exercise { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        public int Age { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    }
}
